using LinearAlgebra
using DataStructures
using DataFrames

using FileIO
using CSVFiles
using BitIntegers

using LinearAlgebra
using QuantumHamiltonian
using TightBindingLattice
using Arpack
using PyCall
npl = pyimport("numpy.linalg")

function make_square_lattice(shape::AbstractMatrix{<:Integer})
    latticevectors = [1.0 0.0; 0.0 1.0]
    unitcell = make_unitcell(latticevectors, OrbitalType=String)
    addorbital!(unitcell, "A", carte2fract(unitcell, [0.0, 0.0]))
    nnbondtypes = [ [1, 0], [0, 1] ]
    nnnbondtypes = [ [ 1, 1], [-1, 1] ]

    lattice = make_lattice(unitcell, shape)
    orthocube = lattice.orthocube
    supercell = lattice.supercell
    tsym = TranslationSymmetry(lattice)
    psym = little_symmetry(tsym, PointSymmetryDatabase.find2d("4mm"))
    tsymbed = embed(lattice, tsym)
    psymbed = embed(lattice, psym)
    ssymbed = tsymbed ⋊ psymbed

    nnbonds = []
    nnnbonds = []

    for r_row in lattice.bravais_coordinates
        for colvec in nnbondtypes
            R_col, r_col = orthocube.wrap(r_row .+ colvec)
            roworb_super = ("A", r_row)
            colorb_super = ("A", r_col)
            irow = get(supercell.orbitalindices, roworb_super, -1)
            icol = get(supercell.orbitalindices, colorb_super, -1)
            push!(nnbonds, ((irow, icol), R_col))
        end
        for colvec in nnnbondtypes
            R_col, r_col = orthocube.wrap(r_row .+ colvec)
            roworb_super = ("A", r_row)
            colorb_super = ("A", r_col)
            irow = get(supercell.orbitalindices, roworb_super, -1)
            icol = get(supercell.orbitalindices, colorb_super, -1)
            push!(nnnbonds, ((irow, icol), R_col))
        end
    end

    return (unitcell=unitcell,
            lattice=lattice,
            space_symmetry_embedding=ssymbed,
            nearest_neighbor_bonds=nnbonds,
            next_nearest_neighbor_bonds=nnnbonds)
end




function compute_square(
                Jx::Real, Jy::Real, Jz ::Real, J2::Real,
                shape::AbstractMatrix{<:Integer},
                Szs::AbstractVector{<:Real},
                tsym_irrep_indices::AbstractVector{<:Integer},
                psym_irrep_indices::AbstractVector{<:Integer},
                psym_irrep_components::AbstractVector{<:Integer},
                max_dense::Integer, 
                nev::Integer,
                force::Bool)
    println("lattice shape: $shape")
    println("Jx: $Jx")
    println("Jy: $Jy")
    println("Jz: $Jz")
    println("J2: $J2")
    println("Sz: $Szs")

    n11 = shape[1,1]
    n12 = shape[1,2]
    n21 = shape[2,1]
    n22 = shape[2,2]

    parameter = Dict(:shape=>"($n11,$n21)x($n12,$n22)", :Jx=>Jx, :Jy=>Jy, :Jz=>Jz, :J2=>J2)
    if !isempty(Szs)
        parameter[:Sz] = join(unique(sort(Szs)), ",")
    end
    if !isempty(tsym_irrep_indices)
        parameter[:tii] = join(unique(sort(tsym_irrep_indices)), ",")
    end
    if !isempty(psym_irrep_indices)
        parameter[:pii] = join(unique(sort(psym_irrep_indices)), ",")
    end
    if !isempty(psym_irrep_components)
        parameter[:pic] = join(unique(sort(psym_irrep_components)), ",")
    end

    output_filename = savename("spectrum_hybrid_measure", parameter, "csv")
    output_filepath = datadir(joinpath("square-($n11,$n21)x($n12,$n22)", output_filename))
    if ispath(output_filepath)
        println("File $output_filepath exists.")
        if force
            println("Overwriting.")
        else
            println("Quitting.")
            exit(1)
        end
    end
    println("Will save results to ", output_filepath)
    flush(stdout)

    # == Prepare Hilbert space ==
    trilat = make_square_lattice(shape)

    unitcell = trilat.unitcell
    lattice = trilat.lattice
    ssymbed = trilat.space_symmetry_embedding
    nnbonds = trilat.nearest_neighbor_bonds
    nnnbonds = trilat.next_nearest_neighbor_bonds
    
    n_sites = numorbital(trilat.lattice.supercell)

    println("Number of sites: ", n_sites)

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)

    # == Prepare Hamiltonian ==
    jx = NullOperator()
    jy = NullOperator()
    jz = NullOperator()

    jx2 = NullOperator()
    jy2 = NullOperator()
    jz2 = NullOperator()

    for ((irow, icol), _) in nnbonds
        jx += pauli(irow, :x) * pauli(icol, :x)
        jy += pauli(irow, :y) * pauli(icol, :y)
        jz += pauli(irow, :z) * pauli(icol, :z)
    end

    for ((irow, icol), _) in nnnbonds
        jx2 += pauli(irow, :x) * pauli(icol, :x)
        jy2 += pauli(irow, :y) * pauli(icol, :y)
        jz2 += pauli(irow, :z) * pauli(icol, :z)
    end

    hamiltonian = simplify( (Jx*jx + Jy*jy + Jz*jz) + J2*(Jx*jx2 + Jy*jy2 + Jz*jz2) ) * 0.25
    @show isinvariant(hs, ssymbed, hamiltonian)
    sx = 0.5 * sum(pauli(i, :x) for i in 1:n_sites)
    sy = 0.5 * sum(pauli(i, :y) for i in 1:n_sites)
    sz = 0.5 * sum(pauli(i, :z) for i in 1:n_sites)
    spin_squared = simplify(sx*sx + sy*sy + sz*sz)

    println("Quantum numbers: ", quantum_number_sectors(hs) )
    flush(stdout)

    # == Compute
    all_eigenvalues = Float64[]
    all_quantum_numbers = Int[]

    all_tsym_irrep_index = Int[]
    all_tsym_irrep_compo = Int[]
    all_psym_irrep_index = Int[]
    all_psym_irrep_compo = Int[]
    all_matrix_types = String[]
    all_dimensions = Int[]
    all_spin_two = Float64[]
    all_spin_four = Float64[]

    #hsr = represent(hs)
    if isempty(Szs)
        qns = quantum_number_sectors(hs)
    else
        qns = [(round(Int, x*2), ) for x in Szs]
    end

    println("Target quantum numbers: ", qns)
    flush(stdout)

    for qn in qns
        println("- spin_z: ", qn[1]/2)
        flush(stdout)


        hss = HilbertSpaceSector(hs, qn)
        hssr = represent(hss, BR)

        for ssic in get_irrep_components(ssymbed)
            if !isempty(tsym_irrep_indices) && (ssic.normal.irrep_index ∉ tsym_irrep_indices)
                continue
            elseif !isempty(psym_irrep_indices) && (ssic.rest.irrep_index ∉ psym_irrep_indices)
                continue
            elseif !isempty(psym_irrep_components) && (ssic.rest.irrep_component ∉ psym_irrep_components)
                continue
            end

            k = fractional_momentum(ssymbed, ssic.normal.irrep_index)
            rhssr = symmetry_reduce(hssr, ssic)
            dimension(rhssr) == 0 && continue
            println("  - trans symmetry irrep index : ", ssic.normal.irrep_index)
            println("    momentum (in units of 2π)  : ", lattice.unitcell.reducedreciprocallatticevectors * k)
            println("    little point symmetry      : ", symmetry(ssic.rest.symmetry).hermann_mauguin)
            println("    point symmetry irrep index : ", ssic.rest.irrep_index)
            println("    point symmetry irrep compo : ", ssic.rest.irrep_component)
            println("    Hilbert space dimension    : ", dimension(rhssr))
            flush(stdout)

            hilbert_space_dimension = dimension(rhssr)

            hamiltonian_rep = represent(rhssr, hamiltonian)
            spin_squared_rep = represent(rhssr, spin_squared)

            if dimension(rhssr) > max_dense
                println("    matrix type                : sparse")
                flush(stdout)
                eigenvalues, eigenvectors = eigs(hamiltonian_rep; nev=nev, which=:SR)
                eigenvalues = real.(eigenvalues)
                matrix_type = "sparse"
            else
                println("    matrix type                : dense")
                flush(stdout)
                hamiltonian_dense = Matrix(hamiltonian_rep)
                eigenvalues, eigenvectors = npl.eigh(hamiltonian_dense)
                matrix_type = "dense"
            end

            for i_eigen in eachindex(eigenvalues)
                phi = eigenvectors[:, i_eigen]
                s2phi = spin_squared_rep * phi
                s4phi = spin_squared_rep * s2phi
                s2 = real(dot(phi, s2phi))
                s4 = real(dot(phi, s4phi))

                push!(all_eigenvalues, eigenvalues[i_eigen])
                push!(all_quantum_numbers, qn[1])
                push!(all_tsym_irrep_index, ssic.normal.irrep_index)
                push!(all_tsym_irrep_compo, ssic.normal.irrep_component)
                push!(all_psym_irrep_index, ssic.rest.irrep_index)
                push!(all_psym_irrep_compo, ssic.rest.irrep_component)
                push!(all_matrix_types, matrix_type)
                push!(all_spin_two, s2)
                push!(all_spin_four, s4)
                push!(all_dimensions, hilbert_space_dimension)
            end # for i_eigen
        end # for tsic
    end

    df = DataFrame(eigenvalue=all_eigenvalues,
                   Sz=all_quantum_numbers ./ 2,
                   tsym_irrep_index=all_tsym_irrep_index,
                   tsym_irrep_component=all_tsym_irrep_compo,
                   psym_irrep_index=all_psym_irrep_index,
                   psym_irrep_component=all_psym_irrep_compo,
                   dimension=all_dimensions,
                   matrix_type=all_matrix_types)

    mkpath(dirname(output_filepath); mode=0o755)
    save(output_filepath, df)
end
