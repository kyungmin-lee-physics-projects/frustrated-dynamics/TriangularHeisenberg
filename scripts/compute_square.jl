using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using ArgParse

include(srcdir("Square.jl"))

function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "--shape"
        arg_type = Int
        nargs = 4
        required = true
        "--Jx"
        arg_type = Float64
        default = 1.0
        "--Jy"
        arg_type = Float64
        default = 1.0
        "--Jz"
        arg_type = Float64
        required = true
        "--J2"
        arg_type = Float64
        default = 0.0
        "--Sz"
        arg_type = Float64
        nargs = '*'
        help = "values of Sz to consider"
        "--tii"
        arg_type = Int64
        nargs = '*'
        help = "translation symmetry irrep index (momentum index)"
        "--pii"
        arg_type = Int64
        nargs = '*'
        help = "(little) point symmetry irrep index"
        "--pic"
        arg_type = Int64
        nargs = '*'
        help = "(little) point symmetry irrep component"
        "--max-dense"
        arg_type = Int64
        default = 20000
        help = "maximum hilbert space dimension to solve with dense matrix"
        "--nev"
        arg_type = Int
        default = 300
        help = "number of eigenvalues to compute when using sparse"
        "--force", "-f"
        help = "force run (overwrite)"
        action = :store_true
    end
    return parse_args(s)
end


function main()

    parsed_args = parse_commandline()
    ss = parsed_args["shape"]
    shape = [ss[1] ss[3]; ss[2] ss[4]]

    Jx = parsed_args["Jx"]
    Jy = parsed_args["Jy"]
    Jz = parsed_args["Jz"]
    J2 = parsed_args["J2"]

    Sz = parsed_args["Sz"]
    tii = parsed_args["tii"]
    pii = parsed_args["pii"]
    pic = parsed_args["pic"]

    max_dense = parsed_args["max-dense"]
    nev = parsed_args["nev"]

    compute_square(Jx, Jy, Jz, J2, shape, Sz, tii, pii, pic, max_dense, nev, parsed_args["force"])
end

main()
