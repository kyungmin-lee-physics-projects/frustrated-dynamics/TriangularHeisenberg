using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

include(srcdir("Preamble.jl"))
include(srcdir("Triangular.jl"))

using Random
using LinearAlgebra
using DataStructures

using Logging
using Printf
using Formatting
using ArgParse

using CodecXz
using MsgPack
#using JSON
using BitIntegers

using TightBindingLattice
using QuantumHamiltonian
using Arpack

using PyCall
npl = pyimport("numpy.linalg")


function compute_disordered_triangular(
        Jx::Real, Jy::Real, Jz ::Real,
        J1min::Real, J1max::Real,
        J2min::Real, J2max::Real,
        shape::AbstractMatrix{<:Integer},
        Szs::AbstractVector{<:Real},
        max_dense::Integer, nev::Integer,
        random_seed::Integer,
        measure::Bool,
        force::Bool=true)
    
    log("lattice shape: $shape")
    log("Jx: $Jx")
    log("Jy: $Jy")
    log("Jz: $Jz")
    log("J1: $J1min -- $J1max")
    log("J2: $J2min -- $J2max")
    log("Sz: $Szs")

    n11 = shape[1,1]
    n12 = shape[1,2]
    n21 = shape[2,1]
    n22 = shape[2,2]

    triangular = make_triangular_lattice(shape)

    n_sites = numsite(triangular.lattice.supercell)
    log("Number of sites: $n_sites")

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)

    sx = 0.5 * sum(pauli(i, :x) for i in 1:n_sites)
    sy = 0.5 * sum(pauli(i, :y) for i in 1:n_sites)
    sz = 0.5 * sum(pauli(i, :z) for i in 1:n_sites)
    spin_squared = simplify(sx*sx + sy*sy + sz*sz)

    log("Quantum numbers: $(quantum_number_sectors(hs))")

    if isempty(Szs)
        qns = quantum_number_sectors(hs)
    else
        qns = [(round(Int, x*2), ) for x in Szs]
    end

    log("Target quantum numbers: $qns")

    vrange1 = [J1min, J1max]
    vrange2 = [J2min, J2max]

    for (qn,) in qns
        Sz = qn/2
        log("spin_z: $Sz")

        parameter_filename = Dict{Symbol, Any}(
                                :shape=>"($n11,$n21)x($n12,$n22)",
                                :Jx=>@sprintf("%.3f", Jx),
                                :Jy=>@sprintf("%.3f", Jy),
                                :Jz=>@sprintf("%.3f", Jz),
                                :J1=>@sprintf("(%.3f,%.3f)", J1min, J1max),
                                :J2=>@sprintf("(%.3f,%.3f)", J2min, J2max),
                                :Sz=>@sprintf("%.1f", Sz),
                                :seed=>random_seed,
                                )
        if measure
            output_filename = savename("spectrum-disordered-dense-measure", parameter_filename, "msgpack.xz")
        else
            output_filename = savename("spectrum-disordered-dense", parameter_filename, "msgpack.xz")
        end
        output_filepath = datadir("disorder", "($n11,$n21)x($n12,$n22)", output_filename)
        if ispath(output_filepath)
            log("File $output_filepath exists.")
            if force
                log("Overwriting.")
            else
                log("Skipping.")
                continue
            end
        end

        log("Creating Hilbert space sector")
        hss = HilbertSpaceSector(hs, qns)
        hssr = represent_dict(hss)
        hilbert_space_dimension = dimension(hssr)
        log("Hilbert space dimension: $hilbert_space_dimension")

        hilbert_space_dimension == 0 && continue
        log("Will save results to $output_filepath")

        if hilbert_space_dimension > max_dense
            matrix_type = "sparse"
        else
            matrix_type = "dense"
        end
        log("matrix type: $matrix_type")

        spin_squared_rep = represent(hssr, spin_squared)
    
        log("Generating Hamiltonian")
        rng = MersenneTwister(random_seed)

        jx, jy, jz = NullOperator(), NullOperator(), NullOperator()
        for (tri, sgn) in triangular.nearest_neighbor_triangles
            ampl = J1min + (J1max - J1min) * rand(rng, Float64)
            for ((i, j), R) in tri
                jx += pauli(i, :x) * pauli(j, :x) * ampl
                jy += pauli(i, :y) * pauli(j, :y) * ampl
                jz += pauli(i, :z) * pauli(j, :z) * ampl
            end
        end
        j1 = simplify(Jx*jx + Jy*jy + Jz*jz) * 0.25

        @assert J2min == 0
        @assert J2max == 0
        #=
        jx2, jy2, jz2 = NullOperator(), NullOperator(), NullOperator()
        for (tri, sgn) in kagome.next_nearest_neighbor_triangles
            ampl = J2min + (J2max - J2min) * rand(rng, Float64)
            for ((i, j), R) in tri
                jx2 += pauli(i, :x) * pauli(j, :x) * ampl
                jy2 += pauli(i, :y) * pauli(j, :y) * ampl
                jz2 += pauli(i, :z) * pauli(j, :z) * ampl
            end
        end
        j2 = simplify(Jx*jx2 + Jy*jy2 + Jz*jz2) * 0.25

        hamiltonian_rep = represent(hssr, simplify(j1 + j2))
        =#
        hamiltonian_rep = represent(hssr, simplify(j1))

        if matrix_type == "sparse"
            log("Diagonalizing sparse Hamiltonian")
            eigenvalues, eigenvectors = eigs(hamiltonian_rep; nev=nev, which=:SR)
            eigenvalues = real.(eigenvalues)
        else  # if matrix_type == "dense"
            log("Creating dense Hamiltonian matrix")
            hamiltonian_dense = Matrix(hamiltonian_rep)
            log("Diagonalizing dense Hamiltonian matrix")
            eigenvalues, eigenvectors = npl.eigh(hamiltonian_dense)
        end

        spin_two_list = Float64[]
        spin_four_list = Float64[]
        if measure
            spin_two_list = zeros(Float64, length(eigenvalues))
            spin_four_list = zeros(Float64, length(eigenvalues))

            Threads.@threads for i_eigen in eachindex(eigenvalues)
                phi = eigenvectors[:, i_eigen]
                s2phi = spin_squared_rep * phi
                s4phi = spin_squared_rep * s2phi
                s2 = real(dot(phi, s2phi))
                s4 = real(dot(phi, s4phi))

                spin_two_list[i_eigen] = s2
                spin_four_list[i_eigen] = s4
            end # for i_eigen
        end

        log("Saving to $output_filepath")

        mkpath(dirname(output_filepath); mode=0o755)

        open(output_filepath, "w") do io
            ioc = XzCompressorStream(io)
            save_data = OrderedDict(
                "parameter" => OrderedDict("J1" => [J1min, J1max],
                                           "J2" => [J2min, J2max],
                                           "Jx" => Jx,
                                           "Jy" => Jy,
                                           "Jz" => Jz,
                                           "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]],
                                           "seed" => random_seed,
                                ),
                "sector" => OrderedDict("Sz"=>Sz,
                                        "dimension"=> hilbert_space_dimension,
                                        "matrix_type"=>"dense",
                                ),
                "eigenvalue" => eigenvalues,
            )
            if measure
                save_data["spin_two"] = spin_two_list
                save_data["spin_four"] = spin_four_list
            end

            MsgPack.pack(ioc, save_data)
            close(ioc)
        end
    end
end





function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "--shape"
        arg_type = Int
        nargs = 4
        required = true
        help = "shape of the cluster in units of lattice vectors (a,b) x (c,d)"
        "--Jx"
        arg_type = Float64
        nargs = '*'
        default = [1.0]
        help = "values of Jx"
        "--Jy"
        arg_type = Float64
        nargs = '*'
        default = [1.0]
        help = "values of Jy"
        "--Jz"
        arg_type = Float64
        nargs = '+'
        help = "values of Jz"
        "--J1"
        arg_type = Float64
        nargs = 2
        required = true
        help = "values of J1"
        "--J2"
        arg_type = Float64
        nargs = 2
        required = true
        help = "values of J2"
        "--Sz"
        arg_type = Float64
        nargs = '*'
        help = "values of Sz to consider"
        "--max-dense"
        arg_type = Int64
        default = 20000
        help = "maximum hilbert space dimension to solve with dense matrix"
        "--nev"
        arg_type = Int
        default = 512
        help = "number of eigenvalues to compute when using sparse"
        "--seed"
        arg_type = Int
        default = 1
        help = "random seed"
        "--debug", "-d"
        help = "debug"
        action = :store_true
        "--measure", "-m"
        help = "measure"
        action = :store_true
        "--force", "-f"
        help = "force run (overwrite)"
        action = :store_true
    end
    return parse_args(s)
end

function main()
    parsed_args = parse_commandline()

    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    ss = parsed_args["shape"]

    J1 = parsed_args["J1"]
    J2 = parsed_args["J2"]

    Jxs = parsed_args["Jx"]
    Jys = parsed_args["Jy"]
    Jzs = parsed_args["Jz"]

    shape = [ss[1] ss[3]; ss[2] ss[4]]

    Sz = parsed_args["Sz"]

    max_dense = parsed_args["max-dense"]
    nev = parsed_args["nev"]

    random_seed = parsed_args["seed"]
    measure = parsed_args["measure"]
    force = parsed_args["force"]

    for (Jx, Jy, Jz) in Iterators.product(Jxs, Jys, Jzs)
        compute_disordered_triangular(Jx, Jy, Jz, 
                                  J1[1], J1[2],
                                  J2[1], J2[2],
                                  shape, Sz, max_dense, nev, random_seed, measure, force)
    end
end

main()
