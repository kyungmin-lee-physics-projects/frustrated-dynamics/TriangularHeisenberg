using LinearAlgebra
using Plots
import PyPlot
using Glob
using CodecXz
using MsgPack
using Formatting
using ProgressMeter

function get_spacings(values::AbstractVector{<:Real}, digits::Integer=8)
    values2 = round.(sort(values); digits=digits)
    spacings = values2[2:end] - values2[1:end-1]
    return spacings
end

function get_ratios(values::AbstractVector{T}, digits::Integer=8) where {T<:AbstractFloat}
    spacings = get_spacings(values, digits)
    n = length(spacings)
    n < 2 && return T[]
    
    m = n
    while m > 1 && spacings[m] == 0
        m -= 1
    end
    
    m == 1 && return T[]
    
    s_prev = spacings[m] # nonzero
    ratios = T[]
    while m > 1
        m -= 1
        s = spacings[m]
        r = min(s, s_prev) / max(s, s_prev)
        if s != 0
            s_prev = s
        end
        push!(ratios, r)
        #print('mm=', m, 'len =', len(ratios))
    end
    return ratios
end

function pdf_general(beta::Real)
    function pdf(x::Real)
        return (27.0/4) * (x+x^2)^(beta) / (1+x+x^2)^(1+3*beta/2)
    end
    function pdf(x::AbstractArray{<:Real})
        return (27.0/4) .* (x + x.^2).^(beta) ./ (1 + x + x .^ 2)^(1 + 3 .* beta./2)
    end
    return pdf
end

pdf_goe = pdf_general(1)
pdf_poisson(x::Real) = 2 / (1+x)^2
pdf_poisson(x::AbstractArray{<:Real}) = 2 ./ (1+x).^2







function plot(shape_str::AbstractString)
    xs = 0:0.01:1
    ys_goe = pdf_goe.(xs)
    ys_poi = pdf_poisson.(xs)
    
    filepaths = glob("*.msgpack.xz", joinpath(@__DIR__, "..", "data","disorder", shape_str));
    
    database = Dict()
    
    println("Reading $(length(filepaths)) files")
    @showprogress for filepath in filepaths
        open(filepath, "r") do io
            ioc = XzDecompressorStream(io)
            data = MsgPack.unpack(ioc)
            close(ioc)
            parameter = data["parameter"]
            sector = data["sector"]
            J1 = Float64[parameter["J1"]...]
            J2 = Float64[parameter["J2"]...]
            Jx = Float64(parameter["Jx"])
            Jy = Float64(parameter["Jy"])
            Jz = Float64(parameter["Jz"])
            shape = Int[parameter["shape"]...]
            seed = Int(parameter["seed"])
            
            Sz = Float64(sector["Sz"])
            dimension = Int(sector["dimension"])
            matrix_type = String(sector["matrix_type"])
            
            p = (Jx, Jy, Jz, J1, J2, Sz)
            if !haskey(database, p)
                database[p] = Vector{Float64}[]  # just eigenvalues
            end
            push!(database[p], data["eigenvalue"])
        end
    end
    
    fig = PyPlot.figure(figsize=(8, 8))
    portion_sizes = [0.01, 0.02, 0.05,
                     0.10, 0.20, 0.40,
                     0.50, 0.80, 1.00]
    
    panel_shape = [3, 3]
    
    println("Plotting $(length(database)) items")
    @showprogress for ((Jx, Jy, Jz, (J1min, J1max), (J2min, J2max), Sz), all_eigenvalues) in database
        out_filename = format("level-statistics-ratio-portion_J1=({:.3f},{:.3f})_J2=({:.3f},{:.3f})_Jx={:.3f}_Jy={:.3f}_Jz={:.3f}_Sz={:.1f}_shape={:s}.png",
        J1min, J1max, J2min, J2max, Jx, Jy, Jz, Sz, shape_str)
        out_filepath = joinpath(@__DIR__, "..", "plots", "disorder", shape_str, out_filename)
        out_filedir = dirname(out_filepath)
        
        !isdir(out_filedir) && mkpath(out_filedir)
        
        ratios = [Vector{Float64}[] for p in portion_sizes]
        
        dimension = length(first(all_eigenvalues))
        count = length(all_eigenvalues)
        for eigenvalues in all_eigenvalues
            eigenvalue_min = minimum(eigenvalues)
            eigenvalue_max = maximum(eigenvalues)
            idx_select = [(eigenvalues .- eigenvalue_min) .<= (eigenvalue_max - eigenvalue_min) * r for r in portion_sizes]
            for (i, idx) in enumerate(idx_select)
                push!(ratios[i], collect(get_ratios([eigenvalues[idx]...])))
            end
        end
        
        fig.clf()
        axes = fig.subplots(panel_shape..., sharex=true,sharey=true)
        axes_flat = vcat(permutedims(axes, [2, 1])...)
        
        for (i,p) in enumerate(portion_sizes)
            title = "$(round(Int, p*100))%"
            axes_flat[i].hist(vcat(ratios[i]...), bins=100, density=true, alpha=0.75)
            axes_flat[i].set_title(title, fontsize=8)
            axes_flat[i].plot(xs, ys_poi, linewidth=2, label="Poisson")
            axes_flat[i].plot(xs, ys_goe, linewidth=2, label="GOE")
        end
        axes_flat[1].set_xlim(0, 1)
        axes_flat[1].set_ylim(0, 2.2)
        
        axes[end,1].set_xlabel(raw"$\tilde{r}$", fontsize=12)
        axes[end,1].set_ylabel(raw"$P(\tilde{r})$", fontsize=12)
        axes[end,1].legend(loc=1)
        fig.suptitle("Triangular $shape_str. \$ J_{z} = $(Jz), \\quad J_{1} \\in [$(J1min), $(J1max)), \\quad J_{2} \\in [$(J2min), $(J2max)) \$, dim = $(dimension), count = $(count)")
        fig.savefig(out_filepath, dpi=300, bbox_inches="tight")
    end
end

function main()
    for s in ARGS
        plot(s)
    end
end

main()
